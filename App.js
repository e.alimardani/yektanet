import UseBackAndroid from 'hooks/UseBackAndroid';
import React from 'react';
import Store from 'redux/Store';
import Home from 'screens/Home';
import {Provider} from 'react-redux';

const App = () => {
  UseBackAndroid();

  return (
    <Provider store={Store}>
      <Home />
    </Provider>
  );
};
export default App;
