import Header from 'components/Home/Header';
import ProductItem from 'components/Home/ProductItem';
import Empty from 'components/shared/Empty';
import {colors} from 'global/colors';
import UseFilterProduct from 'hooks/UseFilterProduct';
import React, {useEffect, useRef} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {useSelector} from 'react-redux';
import {normalize} from 'utillities/fontSize';

const Home = () => {
  const restaurantList = useSelector(state => state.sort.restaurants);

  const scrollRef = useRef(null);

  const handleScroll = () => {
    scrollRef.current.scrollToOffset({
      offset: 0,
      animated: true,
    });
  };

  useEffect(() => {
    handleScroll();
  }, [restaurantList]);

  UseFilterProduct();

  return (
    <View style={styles.container}>
      <Header
        onChangeCategory={handleScroll}
        totalRestaurant={restaurantList?.length ?? 0}
      />
      <FlatList
        contentContainerStyle={[
          styles.content,
          restaurantList?.length === 0 && styles.empty,
        ]}
        ref={scrollRef}
        keyExtractor={item => item.id.toString()}
        renderItem={ProductItem}
        data={restaurantList}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={<Empty />}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },
  content: {
    paddingHorizontal: normalize(15),
    paddingTop: normalize(15),
    backgroundColor: colors.light,
    flexGrow: 1,
  },
  empty: {
    justifyContent: 'center',
    flex: 1,
  },
});
