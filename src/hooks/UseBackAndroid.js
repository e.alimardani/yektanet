import {useEffect} from 'react';
import {BackHandler, ToastAndroid} from 'react-native';
let backButtonPressedOnceToExit = false;

const UseBackAndroid = () => {
  const handleBackButtonClick = () => {
    if (backButtonPressedOnceToExit) {
      BackHandler.exitApp();
    } else {
      backButtonPressedOnceToExit = true;
      ToastAndroid.show(
        'برای خروج یکبار دیگر دکمه بازگشت را فشار دهید',
        ToastAndroid.LONG,
      );
      setTimeout(() => {
        backButtonPressedOnceToExit = false;
      }, 2000);
      return true;
    }
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);
  return false;
};

export default UseBackAndroid;
