import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setSortFilter} from 'redux/Actions';
import {SET_FILTER} from 'redux/Actions/Type';
import staticRestaurant from 'sourceData/data.json';

const UseFilterProduct = () => {
  const dispatch = useDispatch();

  const has_coupon = useSelector(state => state.sort.filters[0].active);
  const discountValueForView = useSelector(
    state => state.sort.filters[1].active,
  );
  const is_express = useSelector(state => state.sort.filters[2].active);
  const delivery_fee = useSelector(state => state.sort.filters[3].active);
  const is_economical = useSelector(state => state.sort.filters[4].active);
  const categorySelected = useSelector(state => state.sort.categorySelected);
  const is_alphabet_sort = useSelector(state => state.sort.isAlphabetSort);

  const couponFilter = arr => {
    if (has_coupon) {
      return arr.filter(item => item.has_coupon);
    } else {
      return arr;
    }
  };
  const discountFilter = arr => {
    if (discountValueForView) {
      return arr.filter(item => item.discountValueForView > 0);
    } else {
      return arr;
    }
  };

  const expressFilter = arr => {
    if (is_express) {
      return arr.filter(item => item.is_express);
    } else {
      return arr;
    }
  };

  const deliveryFilter = arr => {
    if (delivery_fee) {
      return arr.filter(item => item.delivery_fee === 0);
    } else {
      return arr;
    }
  };

  const economicalFilter = arr => {
    if (is_economical) {
      return arr.filter(item => item.is_economical);
    } else {
      return arr;
    }
  };

  const sortFilter = arr => {
    if (is_alphabet_sort === true) {
      return arr.sort((a, b) => a.title.localeCompare(b.title));
    } else if (is_alphabet_sort === false) {
      return arr.sort((a, b) => b.rating - a.rating);
    } else {
      return arr;
    }
  };

  const categoryFilter = arr => {
    if (!categorySelected || categorySelected.value === 0) {
      return arr;
    } else {
      return arr.filter(item =>
        item.description.includes(categorySelected.title),
      );
    }
  };

  useEffect(() => {
    let result = staticRestaurant;
    result = couponFilter(result);
    result = discountFilter(result);
    result = expressFilter(result);
    result = deliveryFilter(result);
    result = economicalFilter(result);
    result = categoryFilter(result);
    result = sortFilter([...result]);
    dispatch(setSortFilter({type: SET_FILTER, payload: result}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    has_coupon,
    discountValueForView,
    is_express,
    delivery_fee,
    is_economical,
    categorySelected,
    is_alphabet_sort,
  ]);
};
export default UseFilterProduct;
