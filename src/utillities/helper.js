import {LayoutAnimation} from 'react-native';

export const customAnim = num =>
  LayoutAnimation.create(
    num,
    LayoutAnimation.Types.easeInEaseOut,
    LayoutAnimation.Properties.opacity,
  );

export const formatNumber = (value = null) => {
  if (value) {
    return `${value}`.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }
  return '';
};
