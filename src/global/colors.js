export const colors = {
  white: '#fff',
  black: '#000',
  light: '#f8f6f8',
  green: '#36b554',
  hardGreen: '#016b16',
  lightGreen: '#ebffef',
  gray: '#a9a9a9',
  pink: '#ec008c',
};
