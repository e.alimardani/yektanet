export const fonts = {
  regular: 'IRANSansMobile',
  medium: 'IRANSansMobile-Medium',
  bold: 'IRANSansMobile-Bold',
  faNum: 'IRANSansMobileFaNum',
  faNumBold: 'IRANSansFaNum-Bold',
};
