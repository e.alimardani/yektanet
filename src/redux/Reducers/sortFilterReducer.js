import {
  SET_CATEGORY_ITEM,
  SET_CATEGORY_LIST,
  SORT_BY_ALPHABET,
  SET_FILTER,
  SET_FILTER_FLAG,
} from 'redux/Actions/Type';
import restaurantList from 'sourceData/data.json';
import categories from 'sourceData/categories.json';
import filter from 'sourceData/filters.json';

const staticHead = {
  value: 0,
  isRoot: true,
  isHead: true,
  title: 'همه دسته‌ها',
};

const initialState = {
  restaurants: restaurantList,
  categories: [staticHead].concat(categories),
  categorySelected: staticHead,
  filters: filter,
  isAlphabetSort: null,
};

const sortFilterReducer = (state = initialState, action = {}) => {
  const {type, payload} = action;
  switch (type) {
    case SORT_BY_ALPHABET:
      return {
        ...state,
        isAlphabetSort: payload,
      };
    case SET_CATEGORY_LIST:
      return {
        ...state,
        categories: payload ?? initialState.categories,
      };
    case SET_CATEGORY_ITEM:
      return {
        ...state,
        categorySelected: payload ?? initialState.categorySelected,
      };
    case SET_FILTER_FLAG:
      return {
        ...state,
        filters: payload,
      };
    case SET_FILTER:
      return {
        ...state,
        restaurants: payload,
      };
    default:
      return state;
  }
};
export default sortFilterReducer;
