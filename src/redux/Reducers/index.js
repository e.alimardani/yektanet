import {combineReducers} from 'redux';
import sort from './sortFilterReducer';

export default combineReducers({
  sort,
});
