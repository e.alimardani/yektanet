import IconApp from 'components/shared/IconApp';
import ModalApp from 'components/shared/ModalApp';
import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import {images} from 'global/images';
import React, {useState} from 'react';
import {
  Image,
  InteractionManager,
  StatusBar,
  TouchableOpacity,
  View,
} from 'react-native';
import {StyleSheet} from 'react-native';
import {fontSize, normalize} from 'utillities/fontSize';
import SortContent from './SortContent';
import {useDispatch} from 'react-redux';
import {setSortFilter} from 'redux/Actions';
import {SORT_BY_ALPHABET} from 'redux/Actions/Type';
import CategoryContent from './CategoryContent';
import Category from './Category';
import FilterContent from './FilterContent';

const Header = props => {
  const {totalRestaurant, onChangeCategory} = props;
  const [isSortModal, setIsSortModal] = useState(false);
  const [isCategoryModal, setIsCategoryModal] = useState(false);
  const [isFilterModal, setIsFilterModal] = useState(false);
  const [selectedSort, setSelectedSort] = useState(null);

  const dispatch = useDispatch();

  const changeSort = (index, name) => {
    setSelectedSort({index, name});
    dispatch(
      setSortFilter({
        type: SORT_BY_ALPHABET,
        payload: index === 0 ? true : false,
      }),
    );
    InteractionManager.runAfterInteractions(() => {
      setIsSortModal(false);
    });
  };

  const changeCategory = item => {
    InteractionManager.runAfterInteractions(() => {
      setIsCategoryModal(item ? false : true);
    });
  };

  return (
    <View>
      <StatusBar
        animated
        barStyle="dark-content"
        backgroundColor={colors.white}
      />
      <Image resizeMode="contain" style={styles.logo} source={images.logo} />
      <Category
        onFilter={() => setIsFilterModal(true)}
        onChange={onChangeCategory}
        onPress={changeCategory}
      />
      <View style={styles.filter}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => setIsSortModal(true)}
          style={styles.sort}>
          <TextApp
            style={styles.sortText}
            size={fontSize.medium}
            font={fonts.medium}>
            {selectedSort ? selectedSort.name : 'به ترتیب...'}
          </TextApp>
          <IconApp
            color={colors.green}
            size={normalize(24)}
            name="list-outline"
          />
        </TouchableOpacity>
        <TextApp size={fontSize.xlarge} font={fonts.faNumBold}>
          {totalRestaurant} رستوران باز
        </TextApp>
      </View>
      <ModalApp isVisible={isSortModal} onClose={() => setIsSortModal(false)}>
        <SortContent
          selectedIndex={selectedSort?.index}
          onPress={changeSort}
          onClose={() => setIsSortModal(false)}
        />
      </ModalApp>
      <ModalApp
        isVisible={isCategoryModal}
        onClose={() => setIsCategoryModal(false)}>
        <CategoryContent
          onPress={changeCategory}
          onChange={onChangeCategory}
          onClose={() => setIsCategoryModal(false)}
        />
      </ModalApp>
      <ModalApp
        isVisible={isFilterModal}
        onClose={() => setIsFilterModal(false)}>
        <FilterContent onClose={() => setIsFilterModal(false)} />
      </ModalApp>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  logo: {
    alignSelf: 'center',
    height: normalize(50),
    marginVertical: normalize(10),
  },
  filter: {
    marginHorizontal: normalize(15),
    marginBottom: normalize(15),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  sort: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  sortText: {
    color: colors.green,
    marginRight: normalize(5),
  },
});
