import TextApp from 'components/shared/TextApp';
import React, {useRef} from 'react';
import {ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {fontSize, normalize} from 'utillities/fontSize';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import IconApp from 'components/shared/IconApp';
import {useDispatch, useSelector} from 'react-redux';
import {setSortFilter} from 'redux/Actions';
import {SET_CATEGORY_ITEM, SET_CATEGORY_LIST} from 'redux/Actions/Type';

const Category = props => {
  const {onPress, onChange, onFilter} = props;

  const data = useSelector(state => state.sort.categories);
  const filterData = useSelector(state => state.sort.filters);
  const categorySelected = useSelector(state => state.sort.categorySelected);

  let list =
    categorySelected && !categorySelected.isHead ? [categorySelected] : data;

  const dispatch = useDispatch();

  const scrollRef = useRef(null);

  const cancelCategory = () => {
    dispatch(setSortFilter({type: SET_CATEGORY_LIST, payload: null}));
    dispatch(setSortFilter({type: SET_CATEGORY_ITEM, payload: null}));
  };

  const onSelect = item => {
    let Head = {
      value: item.value,
      isRoot: false,
      isHead: true,
      title: item.title,
    };

    let listData = [Head].concat(item.sub);

    if (item?.sub) {
      dispatch(setSortFilter({type: SET_CATEGORY_LIST, payload: listData}));
      dispatch(setSortFilter({type: SET_CATEGORY_ITEM, payload: Head}));
    } else if (item.value === list[0].value) {
      onPress(null);
    } else {
      dispatch(setSortFilter({type: SET_CATEGORY_ITEM, payload: item}));
    }
    onChange();
    scrollRef.current.scrollTo({x: 0, y: 0, animated: true});
  };
  let isFilterSelected = filterData.find(item => item.active);
  return (
    <ScrollView
      showsHorizontalScrollIndicator={false}
      ref={scrollRef}
      style={styles.container}
      contentContainerStyle={styles.content}
      horizontal>
      <>
        <TouchableOpacity
          onPress={onFilter}
          activeOpacity={0.7}
          style={[styles.item, isFilterSelected && styles.itemSelected]}>
          {isFilterSelected && (
            <View style={styles.circle}>
              <TextApp
                style={styles.itemTextSelected}
                font={fonts.faNumBold}
                size={fontSize.small}>
                {filterData.filter(item => item.active).length}
              </TextApp>
            </View>
          )}

          <TextApp
            font={fonts.regular}
            size={fontSize.medium}
            style={[
              styles.itemText,
              isFilterSelected && styles.itemTextSelected,
            ]}>
            {'فیلترها'}
          </TextApp>
          <IconApp
            color={colors.green}
            size={normalize(16)}
            style={[styles.icon, isFilterSelected && styles.itemTextSelected]}
            name="options-outline"
          />
        </TouchableOpacity>
        {list.map(item => {
          let isSelected = item.value === list[0].value && !item.isRoot;

          return (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => onSelect(item)}
              style={[styles.item, isSelected && styles.itemSelected]}
              key={item.value.toString()}>
              {isSelected && (
                <IconApp
                  onPress={cancelCategory}
                  color={colors.white}
                  size={normalize(20)}
                  style={styles.closeIcon}
                  name="close-circle-outline"
                />
              )}
              <TextApp
                font={fonts.regular}
                size={fontSize.medium}
                style={[
                  styles.itemText,
                  isSelected && styles.itemTextSelected,
                ]}>
                {item.title}
              </TextApp>
              {item.value === 0 && (
                <IconApp
                  color={colors.green}
                  size={normalize(16)}
                  style={styles.icon}
                  name="grid-outline"
                />
              )}
            </TouchableOpacity>
          );
        })}
      </>
    </ScrollView>
  );
};

export default Category;

const styles = StyleSheet.create({
  container: {
    transform: [{scaleX: -1}],
  },
  content: {
    marginVertical: normalize(10),
    paddingRight: normalize(10),
    paddingLeft: normalize(15),
  },
  item: {
    borderWidth: 0.5,
    borderRadius: 50,
    borderColor: colors.green,
    paddingVertical: normalize(5),
    paddingHorizontal: normalize(7),
    flexDirection: 'row',
    transform: [{scaleX: -1}],
    alignItems: 'center',
    marginRight: normalize(5),
  },
  itemText: {
    color: colors.green,
    marginLeft: normalize(5),
  },
  icon: {
    marginLeft: normalize(5),
  },
  closeIcon: {
    marginRight: normalize(5),
    marginVertical: -normalize(2),
  },
  itemSelected: {
    backgroundColor: colors.green,
  },
  itemTextSelected: {
    color: colors.white,
  },
  circle: {
    width: normalize(20),
    height: normalize(20),
    borderRadius: normalize(15),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.hardGreen,
  },
});
