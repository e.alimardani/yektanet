import TextApp from 'components/shared/TextApp';
import {fonts} from 'global/fonts';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {fontSize, normalize} from 'utillities/fontSize';
import IconApp from 'components/shared/IconApp';
import {colors} from 'global/colors';
import {useDispatch, useSelector} from 'react-redux';
import SwitchApp from 'components/shared/SwitchApp';
import {setSortFilter} from 'redux/Actions';
import {SET_FILTER_FLAG} from 'redux/Actions/Type';
import filter from 'sourceData/filters.json';
import UseFilterProduct from 'hooks/UseFilterProduct';

const FilterContent = props => {
  const {onClose} = props;
  const data = useSelector(state => state.sort.filters);

  const dispatch = useDispatch();

  const onFilter = (isActive, index) => {
    filter[index].active = isActive;
    dispatch(setSortFilter({type: SET_FILTER_FLAG, payload: filter}));
  };
  UseFilterProduct();
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <IconApp
          onPress={onClose}
          size={normalize(26)}
          name="close-circle-outline"
        />
        <TextApp font={fonts.bold} size={fontSize.xlarge}>
          فیلترها
        </TextApp>
      </View>

      {data.map((item, index) => {
        return (
          <View style={styles.item} key={index.toString()}>
            <SwitchApp
              active={item.active}
              onChange={isActive => onFilter(isActive, index)}
            />
            <TextApp
              style={styles.title}
              font={fonts.regular}
              size={fontSize.large}>
              {item.name}
            </TextApp>
          </View>
        );
      })}
    </View>
  );
};

export default FilterContent;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(15),
    paddingTop: normalize(15),
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: normalize(20),
  },
  item: {
    paddingVertical: normalize(10),
    borderBottomColor: colors.light,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  icon: {
    marginLeft: normalize(5),
  },
  title: {
    flex: 1,
  },
});
