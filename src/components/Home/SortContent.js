import TextApp from 'components/shared/TextApp';
import {fonts} from 'global/fonts';
import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {fontSize, normalize} from 'utillities/fontSize';
import list from 'sourceData/sortList.json';
import IconApp from 'components/shared/IconApp';
import {colors} from 'global/colors';

const SortContent = props => {
  const {onClose, selectedIndex, onPress} = props;

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <IconApp
          onPress={onClose}
          size={normalize(26)}
          name="close-circle-outline"
        />
        <TextApp font={fonts.bold} size={fontSize.xlarge}>
          به ترتیب...
        </TextApp>
      </View>

      {list.map((item, index) => {
        return (
          <TouchableOpacity
            onPress={() => onPress(index, item.name)}
            style={styles.item}
            key={index.toString()}>
            <TextApp font={fonts.regular} size={fontSize.large}>
              {item.name}
            </TextApp>
            {selectedIndex === index && (
              <IconApp
                color={colors.green}
                style={styles.icon}
                size={normalize(26)}
                name="checkmark-sharp"
              />
            )}
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default SortContent;

const styles = StyleSheet.create({
  container: {
    padding: normalize(15),
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: normalize(20),
  },
  item: {
    paddingVertical: normalize(10),
    borderBottomColor: colors.light,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  icon: {
    marginLeft: normalize(5),
  },
});
