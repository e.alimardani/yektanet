import IconApp from 'components/shared/IconApp';
import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import {shadows} from 'global/shadows';
import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {fontSize, normalize} from 'utillities/fontSize';
import {formatNumber} from 'utillities/helper';

const productItem = ({item}) => {
  const {
    title,
    featured,
    backgroundImage,
    description,
    address,
    rating,
    delivery_fee,
    discountValueForView,
  } = item;

  return (
    <View style={styles.container}>
      <Image
        resizeMode="cover"
        style={styles.coverImage}
        source={{uri: backgroundImage}}
      />
      <View style={styles.content}>
        <View style={styles.logo}>
          <Image
            resizeMode="contain"
            style={styles.logoImage}
            source={{uri: featured}}
          />
        </View>
        <View style={styles.headerTitle}>
          <View style={styles.star}>
            <IconApp color={colors.green} name="star" />
            <TextApp
              style={styles.starText}
              font={fonts.faNum}
              size={fontSize.small}>
              {parseFloat(rating).toFixed(1)}
            </TextApp>
          </View>
          <View style={styles.titleWrapper}>
            {discountValueForView > 0 && (
              <View style={styles.discount}>
                <TextApp
                  font={fonts.faNum}
                  size={fontSize.small}
                  style={styles.discountText}>
                  تا {discountValueForView}٪
                </TextApp>
              </View>
            )}
            <TextApp
              style={styles.title}
              font={fonts.medium}
              size={fontSize.medium}>
              {title}
            </TextApp>
          </View>
        </View>
        <TextApp font={fonts.regular} size={fontSize.small}>
          {description}
        </TextApp>
        <TextApp
          style={styles.shadowColor}
          font={fonts.faNum}
          size={fontSize.small}>
          آدرس:
          <TextApp font={fonts.faNum} size={fontSize.small}>
            {' ' + address}
          </TextApp>
        </TextApp>
        <TextApp
          style={styles.shadowColor}
          font={fonts.regular}
          size={fontSize.small}>
          پیک فروشنده:
          <TextApp font={fonts.faNum} size={fontSize.small}>
            {' ' + formatNumber(delivery_fee) + ' تومان'}
          </TextApp>
        </TextApp>
      </View>
    </View>
  );
};

export default productItem;

const styles = StyleSheet.create({
  container: {
    marginBottom: normalize(15),
    backgroundColor: colors.white,
    borderRadius: 5,
    flex: 1,
    overflow: 'hidden',
    ...shadows.ShadowTwo,
  },
  content: {
    padding: normalize(20),
  },
  coverImage: {
    flex: 1,
    height: normalize(150),
  },
  logoImage: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.light,
  },
  logo: {
    width: normalize(60),
    height: normalize(60),
    position: 'absolute',
    ...shadows.ShadowTwo,
    borderRadius: 5,
    padding: normalize(5),
    backgroundColor: colors.white,
    right: normalize(10),
    top: -normalize(45),
  },
  title: {},
  headerTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: normalize(15),
  },
  titleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  star: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.lightGreen,
    paddingHorizontal: normalize(5),
    borderRadius: 3,
  },
  starText: {
    marginTop: 1,
    color: colors.green,
    marginLeft: normalize(5),
  },
  discountText: {
    color: colors.white,
  },
  discount: {
    marginRight: normalize(5),
    borderRadius: 5,
    backgroundColor: colors.pink,
    paddingHorizontal: normalize(5),
  },
  shadowColor: {
    color: colors.gray,
    marginTop: normalize(5),
    lineHeight: 20,
  },
});
