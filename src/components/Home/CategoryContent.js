import TextApp from 'components/shared/TextApp';
import {fonts} from 'global/fonts';
import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {fontSize, normalize} from 'utillities/fontSize';
import IconApp from 'components/shared/IconApp';
import {colors} from 'global/colors';
import {useDispatch, useSelector} from 'react-redux';
import {setSortFilter} from 'redux/Actions';
import {SET_CATEGORY_ITEM, SET_CATEGORY_LIST} from 'redux/Actions/Type';

const CategoryContent = props => {
  const {onClose, onPress, onChange} = props;
  const data = useSelector(state => state.sort.categories);
  const categorySelected = useSelector(state => state.sort.categorySelected);

  const dispatch = useDispatch();

  const renderImage = (index, item) => {
    if (index === 0) {
      return (
        <IconApp
          style={styles.iconCategory}
          color={colors.green}
          size={20}
          name="grid-outline"
        />
      );
    } else {
      return (
        <Image
          resizeMode="contain"
          style={styles.img}
          source={{uri: item?.image}}
        />
      );
    }
  };

  const cancelCategory = () => {
    dispatch(setSortFilter({type: SET_CATEGORY_LIST, payload: null}));
    dispatch(setSortFilter({type: SET_CATEGORY_ITEM, payload: null}));
  };

  const onSelect = item => {
    let Head = {
      value: item.value,
      isRoot: false,
      isHead: true,
      title: item.title,
    };

    let listData = [Head].concat(item.sub);

    if (item?.sub) {
      dispatch(setSortFilter({type: SET_CATEGORY_LIST, payload: listData}));
      dispatch(setSortFilter({type: SET_CATEGORY_ITEM, payload: Head}));
    } else {
      dispatch(setSortFilter({type: SET_CATEGORY_ITEM, payload: item}));
      onPress(item);
    }
    onChange();
  };

  const renderCategory = () => {
    return data.map((item, index) => {
      let isSelected = categorySelected.value === item.value;
      return (
        <TouchableOpacity
          onPress={() => onSelect(item)}
          style={[styles.item, styles]}
          key={index.toString()}>
          {item?.sub && (
            <IconApp
              size={normalize(20)}
              color={colors.gray}
              name="chevron-back-outline"
            />
          )}
          <TextApp
            style={[styles.titleText, isSelected && styles.greenColor]}
            font={fonts.regular}
            size={fontSize.large}>
            {item.title}
          </TextApp>
          {renderImage(index, item)}
          {isSelected && (
            <IconApp
              color={colors.green}
              style={styles.icon}
              size={normalize(26)}
              name="checkmark-sharp"
            />
          )}
        </TouchableOpacity>
      );
    });
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <IconApp
          onPress={onClose}
          size={normalize(26)}
          name="close-circle-outline"
        />
        {!data[0]?.isRoot ? (
          <TouchableOpacity
            onPress={cancelCategory}
            activeOpacity={0.7}
            style={styles.back}>
            <TextApp font={fonts.regular} size={fontSize.large}>
              بازگشت
            </TextApp>
            <IconApp size={normalize(20)} name="chevron-forward-outline" />
          </TouchableOpacity>
        ) : (
          <TextApp font={fonts.bold} size={fontSize.xlarge}>
            انتخاب دسته‌بندی
          </TextApp>
        )}
      </View>
      {renderCategory()}
    </View>
  );
};

export default CategoryContent;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(15),
    paddingTop: normalize(15),
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: normalize(20),
  },
  item: {
    paddingVertical: normalize(10),
    borderBottomColor: colors.light,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  icon: {
    marginLeft: normalize(5),
  },
  titleText: {
    flex: 1,
    color: colors.black,
  },
  img: {
    width: normalize(35),
    height: normalize(35),
    marginLeft: normalize(10),
  },
  iconCategory: {
    marginLeft: normalize(10),
    marginRight: normalize(5),
  },
  greenColor: {
    color: colors.green,
  },
  back: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
