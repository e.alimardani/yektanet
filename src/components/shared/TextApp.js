import {colors} from 'global/colors';
import React from 'react';
import {Text, StyleSheet} from 'react-native';

const TextApp = props => {
  const {children, font, size, style} = props;
  return (
    <Text
      {...props}
      style={[styles.text, {fontSize: size, fontFamily: font}, style]}>
      {children}
    </Text>
  );
};

export default TextApp;

const styles = StyleSheet.create({
  text: {
    includeFontPadding: false,
    padding: 0,
    color: colors.black,
  },
});
