import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

const IconApp = props => <Icon {...props} />;

export default IconApp;
