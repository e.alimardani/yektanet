import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {fontSize, normalize} from 'utillities/fontSize';
import IconApp from './IconApp';
import TextApp from './TextApp';

const Empty = () => {
  return (
    <View style={styles.container}>
      <IconApp size={normalize(46)} color={colors.gray} name="sad-outline" />
      <TextApp style={styles.title} size={fontSize.xlarge} font={fonts.bold}>
        نتیجه‌ای یافت نشد !
      </TextApp>
    </View>
  );
};
export default Empty;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    color: colors.gray,
    marginTop: normalize(5),
  },
});
