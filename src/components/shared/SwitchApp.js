import {colors} from 'global/colors';
import React, {useEffect, useRef, useState} from 'react';
import {StyleSheet, TouchableOpacity, Animated, Easing} from 'react-native';
import {normalize} from 'utillities/fontSize';
import IconApp from './IconApp';
const Clickable = Animated.createAnimatedComponent(TouchableOpacity);

const SwitchApp = props => {
  const {active, onChange, containerStyle} = props;
  const [isActive, setIsActive] = useState(active);
  const movement = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    handleAnimation();
  });

  const handleChecked = () => {
    onChange(!isActive);
    setIsActive(!isActive);
    handleAnimation();
  };

  const handleAnimation = () => {
    if (isActive) {
      Animated.timing(movement, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true,
        easing: Easing.ease,
      }).start();
    } else {
      Animated.timing(movement, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true,
        easing: Easing.ease,
      }).start();
    }
  };

  return (
    <Clickable
      activeOpacity={0.8}
      style={[
        style.container,
        {
          backgroundColor: isActive ? colors.green : colors.gray,
        },
        containerStyle,
      ]}
      onPress={() => handleChecked()}>
      <Animated.View
        style={[
          style.handle,
          {
            transform: [
              {
                translateX: movement.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, normalize(18)],
                }),
              },
            ],
          },
        ]}>
        {isActive ? (
          <IconApp
            size={normalize(18)}
            color={colors.green}
            name="checkmark-sharp"
          />
        ) : (
          <IconApp
            size={normalize(18)}
            color={colors.gray}
            name="close-sharp"
          />
        )}
      </Animated.View>
    </Clickable>
  );
};
export default SwitchApp;
const style = StyleSheet.create({
  container: {
    width: normalize(50),
    height: normalize(32),
    borderRadius: normalize(30),
    paddingHorizontal: normalize(2),
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  handle: {
    width: normalize(28),
    height: normalize(28),
    borderRadius: normalize(30),
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
